<?php

/**
 * @file
 * Definition of the 'parallax' panel style.
 */

// Plugin definition.
$plugin = array(
  'title' => t('Parallex Background'),
  'render region' => 'ppr_render_region',
  'settings form' => 'ppr_settings_form',
  'hook theme' => array(
    'panels_parallax_box' => array(
      'variables' => array('content' => NULL),
      'path' => drupal_get_path('module', 'ppr') . '/plugins/styles/parallax',
      'template' => 'ppr-box',
    ),
  ),
);

/**
 * Render callback.
 *
 * @ingroup themeable
 */
function theme_ppr_render_region($vars) {
  // Add Stellar JS file.
  drupal_add_js(libraries_get_path('parallax') . '/parallax.min.js');
  drupal_add_js(drupal_get_path('module', 'ppr') . '/plugins/styles/parallax/js/parallax.js');

  $panes = $vars['panes'];
  $settings = $vars['settings'];

  $output = '';

  $print_separator = FALSE;
  foreach ($panes as $pane) {
    // Add the separator if we've already displayed a pane.
    if ($print_separator) {
      $output .= '<div class="panel-separator">&nbsp;</div>';
    }

    $output .= $pane;
    $print_separator = TRUE;
  }

  $style_attributes = array();
  $style_attributes['data-image-src'] = $settings['image_src'];

  if ($settings['natural_width']) {
    $style_attributes['data-natural-width'] = $settings['natural_width'];
  }
  if ($settings['natural_height']) {
    $style_attributes['data-natural-height'] = $settings['natural_height'];
  }
  if ($settings['position_x']) {
    $style_attributes['data-position-x'] = $settings['position_x'];
  }
  if ($settings['position_y']) {
    $style_attributes['data-position-y'] = $settings['position_y'];
  }
  if ($settings['speed']) {
    $style_attributes['data-speed'] = $settings['speed'];
  }
  if ($settings['z_index']) {
    $style_attributes['data-z-index'] = $settings['z_index'];
  }
  if ($settings['bleed']) {
    $style_attributes['data-bleed'] = $settings['bleed'];
  }
  if ($settings['ios_fix']) {
    $style_attributes['data-ios-fix'] = $settings['ios_fix'];
  }
  if ($settings['android_fix']) {
    $style_attributes['data-android-fix'] = $settings['android_fix'];
  }

  $output = theme('panels_parallax_box', array('content' => $output, 'style_attributes' => $style_attributes));

  return $output;
}

/**
 * Settings form callback.
 */
function ppr_settings_form($style_settings) {
  $form['image_src'] = array(
    '#type' => 'textfield',
    '#title' => t('Image source'),
    '#required' => TRUE,
    '#default_value' => (isset($style_settings['image_src'])) ? $style_settings['image_src'] : '',
    '#description' => t('You must provide a path to the image you wish to apply to the parallax effect.'),
  );

  $form['natural_width'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural width'),
    '#default_value' => (isset($style_settings['natural_width'])) ? $style_settings['natural_width'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
  );

  $form['natural_height'] = array(
    '#type' => 'textfield',
    '#title' => t('Natural height'),
    '#default_value' => (isset($style_settings['natural_height'])) ? $style_settings['natural_height'] : '',
    '#description' => t('You can provide the natural width and natural height of an image to speed up loading and reduce error when determining the correct aspect ratio of the image.'),
  );

  $form['position_x'] = array(
    '#type' => 'textfield',
    '#title' => t('Position X'),
    '#default_value' => (isset($style_settings['position_x'])) ? $style_settings['position_x'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['position_y'] = array(
    '#type' => 'textfield',
    '#title' => t('Position Y'),
    '#default_value' => (isset($style_settings['position_y'])) ? $style_settings['position_y'] : '',
    '#description' => t('This is analogous to the background-position css property. Specify coordinates as top, bottom, right, left, center, or pixel values (e.g. -10px 0px). The parallax image will be positioned as close to these values as possible while still covering the target element.'),
  );

  $form['speed'] = array(
    '#type' => 'textfield',
    '#title' => t('speed'),
    '#default_value' => (isset($style_settings['speed'])) ? $style_settings['speed'] : '',
    '#description' => t('The speed at which the parallax effect runs. 0.0 means the image will appear fixed in place, and 1.0 the image will flow at the same speed as the page content.'),
  );

  $form['z_index'] = array(
    '#type' => 'textfield',
    '#title' => t('Z Index'),
    '#default_value' => (isset($style_settings['z_index'])) ? $style_settings['z_index'] : '',
    '#description' => t('The z-index value of the fixed-position elements. By default these will be behind everything else on the page.'),
  );

  $form['bleed'] = array(
    '#type' => 'textfield',
    '#title' => t('bleed'),
    '#default_value' => (isset($style_settings['bleed'])) ? $style_settings['bleed'] : '',
    '#description' => t('You can optionally set the parallax mirror element to extend a few pixels above and below the mirrored element. This can hide slow or stuttering scroll events in certain browsers.'),
  );

  $form['ios_fix'] = array(
    '#type' => 'textfield',
    '#title' => t('IOS Fix'),
    '#default_value' => (isset($style_settings['ios_fix'])) ? $style_settings['ios_fix'] : '',
    '#description' => t('iOS devices are incompatible with this plugin. If true, this option will set the parallax image as a static, centered background image whenever it detects an iOS user agent. Disable this if you wish to implement your own graceful degradation.'),
  );

  $form['android_fix'] = array(
    '#type' => 'textfield',
    '#title' => t('Android Fix'),
    '#default_value' => (isset($style_settings['android_fix'])) ? $style_settings['android_fix'] : '',
    '#description' => t('If true, this option will set the parallax image as a static, centered background image whenever it detects an Android user agent. Disable this if you wish to enable the parallax scrolling effect on Android devices.'),
  );

  return $form;
}
