(function ($) {
  'use strict';
  Drupal.behaviors.ppr = {
    attach: function (context, settings) {
      $('.ppr-region').parallax();
    }
  };
})(jQuery);
