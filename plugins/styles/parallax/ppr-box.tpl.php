<?php

/**
 * @file
 * Style parallax box and add attributes.
 *
 * - $content: The content of the box.
 * - $style_attributes: The paralax attributes.
 */
?>

<div class="ppr-region" <?php print drupal_attributes($style_attributes); ?>>
  <?php print $content; ?>
</div>
