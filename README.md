# Parallax Panel Regions

## Introduction

**Parallax Panel Regions** is a module that allow parallax 
background style for panels.

##Dependencies

- [Libraries API 2.x](http://drupal.org/project/libraries)
- [Panels](http://drupal.org/project/panels)
- [jQuery Update](https://www.drupal.org/project/jquery_update)
- [Parallax](http://pixelcog.github.io/parallax.js/)

## Installation

1. Download the Parallax library from 
https://github.com/pixelcog/parallax.js/archive/master.zip

2. Extract folder and rename to parallax

3. Put the folder in a libraries directory
    - Ex: sites/all/libraries/parallax/

3. Update 


That's it!

##Usage

1. Enable the module.

2. Now you can find parallax background style on panel style.

That's it!

##drush Make

You can also use Drush Make to download the library automatically.
Simply copy/paste the 'parallax.make.example' to 'parallax.make' 
or copy the contents of the make file into your own make file.

## Contact details
Mohammad Abdul-Qader
mohd@sprintive.com
